----Trail of Nexus6 - Tyrell Soundset by Complex---------------------------------------
----For Tyrell Nexus 6 VSTi by u-he----------------------------------------------------

Thanks for supporting ZenSound!
Please let we know if there are things to improve, change or simply ask a question.

----WARNING!---------------------------------------------------------------------------

Please lower your monitors and headphones volumes if are high, some presets are
designed to work randomly and can harm your equipment if you don't take precautions.

----Installation-----------------------------------------------------------------------

Go to the folder 'Presets' and copy the folder 'Trail of Nexus 6' in:
\vstplugins\TyrellN6.data\Presets\TyrellN6

----Contact----------------------------------------------------------------------------

http://www.zensound.es
https://soundcloud.com/complex_sound
https://twitter.com/zensound_
info@zensound.es

---------------------------------------------------------------------------------------

